variable project {
  description = "Project ID"
  default = "otus-project-282114"
}
variable region {
  description = "Region"
  default     = "europe-west4"
}
variable zone {
  description = "Zone"
  default     = "europe-west4-b"
}
