terraform {
  required_version = "~> 0.12"
}

provider "google" {
  version = "~> 2.15"
  project = var.project
  region  = var.region
}

resource "google_container_cluster" "primary" {
  name     = "otus-project-cluster"
  location = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  enable_legacy_abac = true
  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }
  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials otus-project-cluster --zone europe-west4-b --project otus-project-282114"
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "otus-project-node-pool"
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = 3

  node_config {
    preemptible  = true
    machine_type = "n1-standard-2"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
